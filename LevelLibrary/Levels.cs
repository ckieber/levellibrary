using System;
using System.Collections.Generic;
using System.Text;
using PositionLibrary;
using System.Runtime.Serialization;

namespace LevelLibrary
{
    [Serializable]
    public class Levels
    {
        private List<Level> _levels;

        public Levels()
        {
            _levels = new List<Level>();
        }

        public void addLevel(int levelNum, Position startPos, List<Position> wall, List<Position> box, List<Position> goal)
        {
            Level newLevel = new Level();

            newLevel.setLevelNumber(levelNum);
            newLevel.setStartingPosition(startPos);
            foreach (Position pos in wall)
            {
                newLevel.addWall(pos);
            }
            foreach (Position pos in box)
            {
                newLevel.addBox(pos);
            }
            foreach (Position pos in goal)
            {
                newLevel.addGoal(pos);
            }

            _levels.Add(newLevel);
        }

        public void updateLevel(int levelNum, Position startPos, List<Position> wall, List<Position> box, List<Position> goal)
        {
            Level level = getLevel(levelNum);
            level.clearLists();

            level.setStartingPosition(startPos);
            foreach (Position pos in wall)
            {
                level.addWall(pos);
            }
            foreach (Position pos in box)
            {
                level.addBox(pos);
            }
            foreach (Position pos in goal)
            {
                level.addGoal(pos);
            }
        }

        public void sort()
        {
            _levels.Sort();
        }

        public Level getLevel(int levelNum)
        {
            Level searchLevel = new Level(levelNum);
            if (_levels.Contains(searchLevel))
            {
                int index = _levels.IndexOf(searchLevel);
                return _levels[index];
            }
            else
            {
                return null;
            }
        }

        public int getCount()
        {
            return _levels.Count;
        }

        public bool changeLevelNumber(int oldNum, int newNum)
        {
            Level temp = getLevel(oldNum);
            if (temp != null)
            {
                temp.setLevelNumber(newNum);
                return true;
            }

            return false;
        }

        public bool switchLevelNumbers(int firstLevelNumber, int secondLevelNumber)
        {
            Level first = getLevel(firstLevelNumber);
            Level second = getLevel(secondLevelNumber);
            if (first == null || second == null)
            {
                return false;
            }

            int temp = first.getLevelNumber();
            first.setLevelNumber(second.getLevelNumber());
            second.setLevelNumber(temp);

            return true;
        }

        public bool deleteLevel(int levelNumber)
        {
            bool continuous = false;
            if (isContinuous())
            {
                continuous = true;
            }
            if (_levels.Contains(new Level(levelNumber)))
            {
                int index = _levels.IndexOf(new Level(levelNumber));
                _levels.RemoveAt(index);
                if (levelNumber == 0)
                {                 
                    return true;
                }
                else
                {
                    if (continuous)
                    {
                        //move levels behing forward
                        while (changeLevelNumber(index + 1, index))
                        {
                            index++;
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        public bool isContinuous()
        {
            if (_levels.Count == 0)
            {
                return false;
            }
            else
            {
                int number = _levels[0].getLevelNumber();
                int count = 0;

                while (count < _levels.Count)
                {
                    if (!_levels.Contains(new Level(number)))
                    {
                        return false;
                    }
                    number++;
                    count++;
                }
                return true;
            }
        }

        public void insert(int levelNum, Position startPos, List<Position> wall, List<Position> box, List<Position> goal)
        {
            Level newLevel = new Level();

            newLevel.setLevelNumber(levelNum);
            newLevel.setStartingPosition(startPos);
            foreach (Position pos in wall)
            {
                newLevel.addWall(pos);
            }
            foreach (Position pos in box)
            {
                newLevel.addBox(pos);
            }
            foreach (Position pos in goal)
            {
                newLevel.addGoal(pos);
            }

            //move levels backwards
            moveBack(levelNum, levelNum + 1);

            _levels.Add(newLevel);
            _levels.Sort();
        }

        private void moveBack(int oldLevel, int newLevel)
        {
            if (_levels.Contains(new Level(newLevel)))
            {
                moveBack(newLevel, newLevel + 1);
            }
            changeLevelNumber(oldLevel, newLevel);
        }

        public List<int> getAllLevelNumbers()
        {
            List<int> list = new List<int>();

            foreach(Level level in _levels)
            {
                list.Add(level.getLevelNumber());
            }

            return list;
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("levels", _levels);
        }

        public Levels(SerializationInfo info, StreamingContext ctxt)
        {
            _levels = (List<Level>)info.GetValue("levels", typeof(List<Level>));
        }
    }
}
