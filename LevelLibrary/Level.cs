using System;
using System.Collections.Generic;
using System.Text;
using PositionLibrary;
using System.Runtime.Serialization;

namespace LevelLibrary
{
    [Serializable]
    public class Level : IComparable
    {
        private int _levelNumber;
        private List<Position> _wallFields;
        private List<Position> _boxFields;
        private List<Position> _goalFields;
        private Position _startPos;

        public Level()
        {
            _levelNumber = 0;
            _wallFields = new List<Position>();
            _boxFields = new List<Position>();
            _goalFields = new List<Position>();
            _startPos = new Position();
        }

        public Level(int levelNum)
        {
            _levelNumber = levelNum;
            _wallFields = new List<Position>();
            _boxFields = new List<Position>();
            _goalFields = new List<Position>();
            _startPos = new Position();
        }

        public void setLevelNumber(int level)
        {
            _levelNumber = level;
        }

        public int getLevelNumber()
        {
            return _levelNumber;
        }

        public void clearLists()
        {
            _wallFields.Clear();
            _boxFields.Clear();
            _goalFields.Clear();
        }

        public void addWall(Position pos)
        {
            _wallFields.Add(pos);
        }

        public void addBox(Position pos)
        {
            _boxFields.Add(pos);
        }

        public void addGoal(Position pos)
        {
            _goalFields.Add(pos);
        }

        public void setStartingPosition(Position pos)
        {
            _startPos = pos;
        }

        public List<Position> getWallFields()
        {
            return _wallFields;
        }

        public List<Position> getGoalFields()
        {
            return _goalFields;
        }

        public List<Position> getBoxFields()
        {
            return _boxFields;
        }

        public Position getStartPosition()
        {
            return _startPos;
        }

        public override bool Equals(object obj)
        {
            return _levelNumber.Equals(((Level)obj).getLevelNumber());
        }

        public override int GetHashCode()
        {
            return _levelNumber.GetHashCode();
        }

        public int CompareTo(object obj)
        {
            return _levelNumber.CompareTo(((Level)obj).getLevelNumber());
        }
    }
}
